//Creación de personas
CREATE ( Y: Persona { apellido: "Lopez", nombre: "Yoel" } );
CREATE ( A: Persona { apellido: "Bassi", nombre: "Agustín" } );
CREATE ( D: Persona { apellido: "Broin", nombre: "David" } );
CREATE ( M: Persona { apellido: "Castello", nombre: "Marcelo" } );
CREATE ( J: Persona { apellido: "Castiñeiras", nombre: "José" } );
CREATE ( C: Persona { apellido: "Pantelides", nombre: "Carlos" } );

//Creación de materias
CREATE ( G: Materia { nombre: "Gestión de grandes volúmenes de datos", short_name: "GdV", obligatoria: true } );
CREATE ( I: Materia { nombre: "Infraestructura para la implementación de sistemas", short_name: "IIS", obligatoria: false } );
CREATE ( P: Materia { nombre: "Arquitectura de protocolos", short_name: "AdP", obligatoria: true } );
CREATE ( D: Materia { nombre: "Desarrollo de aplicaciones web", short_name: "DdA", obligatoria: true } );
CREATE ( Ci: Materia { nombre: "Ciberseguridad en internet de las cosas", short_name: "CibS", obligatoria: true } );
CREATE ( T: Materia { nombre: "Testing de Sistemas de Internet de las Cosas", short_name: "TSIoT", obligatoria: false } );

//Creación de relaciones DICTO (docentes)
MATCH (A: Persona { apellido: "Bassi"}), (P: Materia { short_name: "AdP"} ) CREATE (A)-[:DICTO {cohorte: 3, bimestre: 2 } ]->(P);
MATCH (Y: Persona { apellido: "Lopez"}), (G: Materia { short_name: "GdV"} ) CREATE (Y)-[:DICTO {cohorte: 3, bimestre: 3 } ]->(G);
MATCH (Y: Persona { apellido: "Lopez"}), (I: Materia { short_name: "IIS"} ) CREATE (Y)-[:DICTO {cohorte: 3, bimestre: 4 } ]->(I);
MATCH (C: Persona { apellido: "Pantelides"}), (Ci: Materia { short_name: "CibS"} ) CREATE (C)-[:DICTO {cohorte: 3, bimestre: 3 } ]->(Ci);

//Creación de relaciones CURSO (alumnos)
MATCH (B: Persona { apellido: "Broin"}), (P: Materia { short_name: "AdP"} ) CREATE (B)-[:CURSO {calificacion: 8, cohorte: 3, bimestre: 2 } ]->(P);
MATCH (B: Persona { apellido: "Broin"}), (G: Materia { short_name: "GdV"} ) CREATE (B)-[:CURSO {calificacion: 9, cohorte: 3, bimestre: 3, grupo: 1 } ]->(G);
MATCH (M: Persona { apellido: "Castello"}), (G: Materia { short_name: "GdV"} ) CREATE (M)-[:CURSO {calificacion: 9, cohorte: 3, bimestre: 3, grupo: 2 } ]->(G);
MATCH (J: Persona { apellido: "Castiñeiras"}), (G: Materia { short_name: "GdV"} ) CREATE (J)-[:CURSO {calificacion: 9, cohorte: 3, bimestre: 3, grupo: 1 } ]->(G);
MATCH (J: Persona { apellido: "Castiñeiras"}), (T: Materia { short_name: "TSIoT"} ) CREATE (J)-[:CURSO { cohorte: 3, bimestre: 4 } ]->(T);
MATCH (B: Persona { apellido: "Broin"}), (I: Materia { short_name: "IIS"} ) CREATE (B)-[:CURSO { cohorte: 3, bimestre: 4 } ]->(I);
MATCH (M: Persona { apellido: "Castello"}), (I: Materia { short_name: "IIS"} ) CREATE (M)-[:CURSO { cohorte: 3, bimestre: 4 } ]->(I);
MATCH (C: Persona { apellido: "Pantelides"}), (D: Materia { short_name: "DdA"} ) CREATE (C)-[:CURSO { cohorte: 3, bimestre: 1 } ]->(D);

//Creación de relaciones CONOCE
MATCH (B: Persona { apellido: "Broin"}), (M: Persona { apellido: "Castello"} ) CREATE (B)-[:CONOCE]->(M);
MATCH (M: Persona { apellido: "Castello"}), (J: Persona { apellido: "Castiñeiras"} ) CREATE (M)-[:CONOCE]->(J);
MATCH (J: Persona { apellido: "Castiñeiras"}), (A: Persona { apellido: "Bassi"} ) CREATE (J)-[:CONOCE]->(A);
