[[_TOC_]]

# Introducción
En este repositorio se encuentra un conjunto de ejercicios sobre Neo4j.

# Prerequisitos
- El proceso se llevó a cabo utilizando [Neo4j Desktop](https://neo4j.com/download/)
- Se eliminaron todos los nodos y relaciones de la base de datos _default_.

```cypher
MATCH (n) DETACH DELETE n;
```

# Modelo
En el siguiente diagrama se muestran las entidades, relaciones y propiedades que se 
tendrán en cuenta:    
![](img/domain_graph.png)

En el archivo [data.cypher](data.cypher) se puede encontrar el código `cypher`
que se utilizó para la carga de los nodos y relaciones.

## Carga de nodos
1. Ejecutar el siguiente fragmento de código

```cypher
//Creación de personas
CREATE ( Y: Persona { apellido: "Lopez", nombre: "Yoel" } );
CREATE ( A: Persona { apellido: "Bassi", nombre: "Agustín" } );
CREATE ( D: Persona { apellido: "Broin", nombre: "David" } );
CREATE ( M: Persona { apellido: "Castello", nombre: "Marcelo" } );
CREATE ( J: Persona { apellido: "Castiñeiras", nombre: "José" } );
CREATE ( C: Persona { apellido: "Pantelides", nombre: "Carlos" } );

//Creación de materias
CREATE ( G: Materia { nombre: "Gestión de grandes volúmenes de datos", short_name: "GdV", obligatoria: true } );
CREATE ( I: Materia { nombre: "Infraestructura para la implementación de sistemas", short_name: "IIS", obligatoria: false } );
CREATE ( P: Materia { nombre: "Arquitectura de protocolos", short_name: "AdP", obligatoria: true } );
CREATE ( D: Materia { nombre: "Desarrollo de aplicaciones web", short_name: "DdA", obligatoria: true } );
CREATE ( Ci: Materia { nombre: "Ciberseguridad en internet de las cosas", short_name: "CibS", obligatoria: true } );
CREATE ( T: Materia { nombre: "Testing de Sistemas de Internet de las Cosas", short_name: "TSIoT", obligatoria: false } );
```
2. Se puede visualizar los nodos creados utilizando
```cypher
MATCH (n) RETURN n
````
![](img/nodes.png)

## Carga de relaciones
1. Ejecutar el siguiente fragmento de código

```cypher
//Creación de relaciones DICTO (docentes)
MATCH (A: Persona { apellido: "Bassi"}), (P: Materia { short_name: "AdP"} ) CREATE (A)-[:DICTO {cohorte: 3, bimestre: 2 } ]->(P);
MATCH (Y: Persona { apellido: "Lopez"}), (G: Materia { short_name: "GdV"} ) CREATE (Y)-[:DICTO {cohorte: 3, bimestre: 3 } ]->(G);
MATCH (Y: Persona { apellido: "Lopez"}), (I: Materia { short_name: "IIS"} ) CREATE (Y)-[:DICTO {cohorte: 3, bimestre: 4 } ]->(I);
MATCH (C: Persona { apellido: "Pantelides"}), (Ci: Materia { short_name: "CibS"} ) CREATE (C)-[:DICTO {cohorte: 3, bimestre: 3 } ]->(Ci);

//Creación de relaciones CURSO (alumnos)
MATCH (B: Persona { apellido: "Broin"}), (P: Materia { short_name: "AdP"} ) CREATE (B)-[:CURSO {calificacion: 8, cohorte: 3, bimestre: 2 } ]->(P);
MATCH (B: Persona { apellido: "Broin"}), (G: Materia { short_name: "GdV"} ) CREATE (B)-[:CURSO {calificacion: 9, cohorte: 3, bimestre: 3, grupo: 1 } ]->(G);
MATCH (M: Persona { apellido: "Castello"}), (G: Materia { short_name: "GdV"} ) CREATE (M)-[:CURSO {calificacion: 9, cohorte: 3, bimestre: 3, grupo: 2 } ]->(G);
MATCH (J: Persona { apellido: "Castiñeiras"}), (G: Materia { short_name: "GdV"} ) CREATE (J)-[:CURSO {calificacion: 9, cohorte: 3, bimestre: 3, grupo: 1 } ]->(G);
MATCH (J: Persona { apellido: "Castiñeiras"}), (T: Materia { short_name: "TSIoT"} ) CREATE (J)-[:CURSO { cohorte: 3, bimestre: 4 } ]->(T);
MATCH (B: Persona { apellido: "Broin"}), (I: Materia { short_name: "IIS"} ) CREATE (B)-[:CURSO { cohorte: 3, bimestre: 4 } ]->(I);
MATCH (M: Persona { apellido: "Castello"}), (I: Materia { short_name: "IIS"} ) CREATE (M)-[:CURSO { cohorte: 3, bimestre: 4 } ]->(I);
MATCH (C: Persona { apellido: "Pantelides"}), (D: Materia { short_name: "DdA"} ) CREATE (C)-[:CURSO { cohorte: 3, bimestre: 1 } ]->(D);

//Creación de relaciones CONOCE
MATCH (B: Persona { apellido: "Broin"}), (M: Persona { apellido: "Castello"} ) CREATE (B)-[:CONOCE]->(M);
MATCH (M: Persona { apellido: "Castello"}), (J: Persona { apellido: "Castiñeiras"} ) CREATE (M)-[:CONOCE]->(J);
MATCH (J: Persona { apellido: "Castiñeiras"}), (A: Persona { apellido: "Bassi"} ) CREATE (J)-[:CONOCE]->(A);
````
2. Se puede visualizar los nodos y relaciones creados utilizando 
```cypher
MATCH p=()-->() RETURN p
````
![](img/relationships.png)

# Consultas requeridas
**NOTA:** Para evitar una gran cantidad de capturas de pantalla, se utiliza la salida _Text_ de Neo4j.
## 1. Listado de alumnos que cursaron la misma materia, pero en grupos distintos
```cypher
MATCH (a:Persona)-[ac:CURSO]->()<-[bc:CURSO]-(b:Persona)
WHERE ac.grupo <> bc.grupo
RETURN DISTINCT a.nombre, a.apellido, b.nombre, b.apellido;
```
Nos da por resultado:
```txt
╒══════════╤═════════════╤══════════╤═════════════╕
│"a.nombre"│"a.apellido" │"b.nombre"│"b.apellido" │
╞══════════╪═════════════╪══════════╪═════════════╡
│"Marcelo" │"Castello"   │"David"   │"Broin"      │
├──────────┼─────────────┼──────────┼─────────────┤
│"José"    │"Castiñeiras"│"Marcelo" │"Castello"   │
├──────────┼─────────────┼──────────┼─────────────┤
│"David"   │"Broin"      │"Marcelo" │"Castello"   │
├──────────┼─────────────┼──────────┼─────────────┤
│"Marcelo" │"Castello"   │"José"    │"Castiñeiras"│
└──────────┴─────────────┴──────────┴─────────────┘
```
## 2. Listado de personas que dictaron más de una materia
```cypher
MATCH (p:Persona)-[d:DICTO]->()
WITH p, count(d) AS cnt
WHERE cnt > 1
RETURN p;
```
Nos da por resultado:
```txt
╒════════════════════════════════════╕
│"p"                                 │
╞════════════════════════════════════╡
│{"nombre":"Yoel","apellido":"Lopez"}│
└────────────────────────────────────┘
````
## 3. Calculo de mi promedio
```cypher
MATCH (p:Persona)-[c:CURSO]->()
WHERE p.apellido = "Broin"
RETURN avg(c.calificacion);
```
Nos da por resultado:
```txt
╒═════════════════════╕
│"avg(c.calificacion)"│
╞═════════════════════╡
│8.5                  │
└─────────────────────┘
```
## 4. Recomendación de personas que cursaron juntas pero no se conocen
```cypher
MATCH (a:Persona)-[ac:CURSO]->()<-[bc:CURSO]-(b:Persona)
WHERE ac.cohorte = bc.cohorte
AND ac.bimestre = bc.bimestre
AND NOT (a)-[:CONOCE]-(b)
RETURN DISTINCT a.apellido, b.apellido;
```
Nos da por resultado:
```txt
╒═════════════╤═════════════╕
│"a.apellido" │"b.apellido" │
╞═════════════╪═════════════╡
│"Castiñeiras"│"Broin"      │
├─────────────┼─────────────┤
│"Broin"      │"Castiñeiras"│
└─────────────┴─────────────┘
````
## 5.a. Conocidos de mis conocidos hasta longitud 2
```cypher
MATCH (a:Persona)-[s:CONOCE*..2]->(b)
WHERE a.apellido = "Broin"
RETURN DISTINCT a,b;
```
Nos da por resultado:
![](img/conocidos_len2.png)
## 5.b. Conocidos de mis conocidos hasta longitud indefinida 
```cypher
MATCH (a:Persona)-[s:CONOCE*..]->(b)
WHERE a.apellido = "Broin"
RETURN DISTINCT a,b;
````
Nos da por resultado:
![](img/conocidos_leninf.png)
## 6. Alumnos que también son docentes (dictaron y cursaron materias)
```cypher
MATCH (a:Persona)
WHERE EXISTS ((a:Persona)-[:DICTO]-())
AND EXISTS ((a:Persona)-[:CURSO]-())
RETURN a;
````
Nos da por resultado:
```txt
╒═══════════════════════════════════════════╕
│"a"                                        │
╞═══════════════════════════════════════════╡
│{"nombre":"Carlos","apellido":"Pantelides"}│
└───────────────────────────────────────────┘
````
**NOTA:** En el enunciado se solicita tener en cuenta una variante para el modelado enfocado
a esta consulta. Algunas alternativas son:
- Utilizar una propiedad de tipo booleana para marcar si es `Alumno` y otra para saber si es `Docente`.
La consulta debería buscar todas las personas que tienen ambos _flags_ marcados como `true`.
- En lugar de nodos `Persona`, utilizar nodos `Alumno` y `Docente`. En este caso, la consulta
debería buscar nodos `Alumno` y `Docente` que tengan el mismo nombre y apellido.
## 7. Materias optativas que no he cursado y que curso alguien con quien yo he cursado
```cypher
MATCH (a:Persona)-[ac:CURSO]-(am:Materia), (b:Persona)-[bc:CURSO]-(bm:Materia)
WHERE a.apellido = "Broin"
AND NOT EXISTS ((a:Persona)-[:CURSO]-(bm:Materia))
AND ac.cohorte = bc.cohorte
AND ac.bimestre = bc.bimestre
AND bm.obligatoria = false
RETURN bm.nombre;
````
Nos da por resultado:
```txt
╒══════════════════════════════════════════════╕
│"bm.nombre"                                   │
╞══════════════════════════════════════════════╡
│"Testing de Sistemas de Internet de las Cosas"│
└──────────────────────────────────────────────┘
```
## 8. Materias optativas que no he cursado y que curso alguien con quien yo he cursado y es conocido directo o indirecto
```cypher
MATCH (a:Persona)-[ac:CURSO]-(am:Materia), (b:Persona)-[bc:CURSO]-(bm:Materia)
WHERE a.apellido = "Broin"
AND NOT EXISTS ((a:Persona)-[:CURSO]-(bm:Materia))
AND ac.cohorte = bc.cohorte
AND ac.bimestre = bc.bimestre
AND bm.obligatoria = false
AND EXISTS ((a:Persona)-[:CONOCE*..]-(b:Persona))
RETURN bm.nombre;
````
Nos da por resultado:
```txt
╒══════════════════════════════════════════════╕
│"bm.nombre"                                   │
╞══════════════════════════════════════════════╡
│"Testing de Sistemas de Internet de las Cosas"│
└──────────────────────────────────────────────┘
```
## 9. Lista de alumnos que les falta alguna calificación
A continuación se presenta una consulta capaz de detectar si hay alumnos que no han sido calificados
en alguna materia
```cypher
MATCH (p:Persona)-[c:CURSO]-(m:Materia) 
WHERE NOT EXISTS (c.calificacion)
RETURN p.apellido, m.nombre, c.cohorte, c.bimestre
```
Nos da por resultado:
```txt
╒═════════════╤════════════════════════════════════════════════════╤═══════════╤════════════╕
│"p.apellido" │"m.nombre"                                          │"c.cohorte"│"c.bimestre"│
╞═════════════╪════════════════════════════════════════════════════╪═══════════╪════════════╡
│"Broin"      │"Infraestructura para la implementación de sistemas"│3          │4           │
├─────────────┼────────────────────────────────────────────────────┼───────────┼────────────┤
│"Castello"   │"Infraestructura para la implementación de sistemas"│3          │4           │
├─────────────┼────────────────────────────────────────────────────┼───────────┼────────────┤
│"Castiñeiras"│"Testing de Sistemas de Internet de las Cosas"      │3          │4           │
├─────────────┼────────────────────────────────────────────────────┼───────────┼────────────┤
│"Pantelides" │"Desarrollo de aplicaciones web"                    │3          │1           │
└─────────────┴────────────────────────────────────────────────────┴───────────┴────────────┘
```
