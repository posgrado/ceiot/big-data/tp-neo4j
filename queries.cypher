//0. Listado de alumnos (personas que cursan materias)
MATCH (p:Persona)-[:CURSO]->(m:Materia)
RETURN p;

//1. Listado de alumnos que cursaron la misma materia, pero en grupos distintos
MATCH (a:Persona)-[ac:CURSO]->()<-[bc:CURSO]-(b:Persona)
WHERE ac.grupo <> bc.grupo
RETURN DISTINCT a.nombre, a.apellido, b.nombre, b.apellido;

//2. Listado de personas que dictaron más de una materia
MATCH (p:Persona)-[d:DICTO]->()
WITH p, count(d) AS cnt
WHERE cnt > 1
RETURN p;

//3. Calculo de mi promedio
MATCH (p:Persona)-[c:CURSO]->()
WHERE p.apellido = "Broin"
RETURN avg(c.calificacion);

//4. Recomendación de personas que cursaron juntas pero no se conocen
MATCH (a:Persona)-[ac:CURSO]->()<-[bc:CURSO]-(b:Persona)
WHERE ac.cohorte = bc.cohorte
AND ac.bimestre = bc.bimestre
AND NOT (a)-[:CONOCE]-(b)
RETURN DISTINCT a.apellido, b.apellido;

//5.a. Conocidos de mis conocidos hasta longitud 2 
MATCH (a:Persona)-[s:CONOCE*..2]->(b)
WHERE a.apellido = "Broin"
RETURN DISTINCT a,b;

//5.b. Conocidos de mis conocidos hasta longitud indefinida 
MATCH (a:Persona)-[s:CONOCE*..]->(b)
WHERE a.apellido = "Broin"
RETURN DISTINCT a,b;

//6. Alumnos que también son docentes (dictaron y cursaron materias)
MATCH (a:Persona)
WHERE EXISTS ((a:Persona)-[:DICTO]-())
AND EXISTS ((a:Persona)-[:CURSO]-())
RETURN a;

//7. Materias optativas que no he cursado y que curso alguien con quien yo he cursado
MATCH (a:Persona)-[ac:CURSO]-(am:Materia), (b:Persona)-[bc:CURSO]-(bm:Materia)
WHERE a.apellido = "Broin"
AND NOT EXISTS ((a:Persona)-[:CURSO]-(bm:Materia))
AND ac.cohorte = bc.cohorte
AND ac.bimestre = bc.bimestre
AND bm.obligatoria = false
RETURN bm.nombre;

//8. Materias optativas que no he cursado y que curso alguien con quien yo he cursado
//y es conocido directo o indirecto
MATCH (a:Persona)-[ac:CURSO]-(am:Materia), (b:Persona)-[bc:CURSO]-(bm:Materia)
WHERE a.apellido = "Broin"
AND NOT EXISTS ((a:Persona)-[:CURSO]-(bm:Materia))
AND ac.cohorte = bc.cohorte
AND ac.bimestre = bc.bimestre
AND bm.obligatoria = false
AND EXISTS ((a:Persona)-[:CONOCE*..]-(b:Persona))
RETURN bm.nombre;

